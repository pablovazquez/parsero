#!/usr/bin/env python

import argparse
import datetime
import git
import pytz
from pathlib import Path

# Replace with your timezone.
# List of timezones https://mljar.com/blog/list-pytz-timezones/
timezone = pytz.timezone('Europe/Amsterdam')

# Folder where to write the commit logs.
commit_logs_folder = "commit-logs"

# Set hour for the date-range. (24H format)
time_hour = 14


# ============================================================================#
# * No need to edit below this line. You can if you want, it's open source. * #
# ============================================================================#

notes_content = []

# Stats.
nr_commits = 0
nr_commits_fix = 0
nr_commits_cleanup = 0

# Build the date format based on arguments, timezone-aware.
def build_date(date_string):
    date = datetime.datetime.strptime(date_string, '%Y-%m-%d')
    date = date.replace(hour=time_hour, tzinfo=timezone)
    return date

parser=argparse.ArgumentParser()
parser.add_argument('repository')
parser.add_argument('date_start',type=build_date)
parser.add_argument('date_end',type=build_date)
args=parser.parse_args()
prev_live_time = args.date_start
next_live_time = args.date_end

print(f"\n# Changelog between {prev_live_time.strftime('%B %d, %Y')} - {next_live_time.strftime('%B %d, %Y')}")

# File where to write the notes.
Path(commit_logs_folder).mkdir(parents=True, exist_ok=True)
notes_file = f'{commit_logs_folder}/commits-{prev_live_time.strftime("%Y-%m-%d")}__{next_live_time.strftime("%Y-%m-%d")}.md'

# Open the repository.
repo_path = args.repository
repo = git.Repo(repo_path)

# Get the list of commits.
commits = list(repo.iter_commits("main", reverse=True))

commit_groups = {}
for commit in commits:
    commit_time = commit.committed_datetime.astimezone(timezone)
    if ((commit_time > prev_live_time) and (commit_time < next_live_time)):
        author = commit.author.name
        title = commit.summary.split(":")[0]
        summary = commit.summary
        commit_hash = commit.hexsha[:8]
        date = commit.committed_datetime.strftime('%y-%m-%d')

        if title in commit_groups:
            commit_groups[title].append((commit_hash, summary, author, date))
        else:
            commit_groups[title] = [(commit_hash, summary, author, date)]

# Add each commit to notes_content.
for title, group in sorted(commit_groups.items()):
    # Skip merge commits.
    merge_titles = ['Merge branch ', 'Merge remote']
    if title.startswith(tuple(merge_titles)):
        continue

    if title.startswith('Cleanup'):
        nr_commits_cleanup = len(group)

    if title.startswith('Fix '):
        is_fix = True
        nr_commits_fix += 1
    else:
        p_title = f"\n### {title}"
        notes_content.append(p_title)

    for commit_hash, summary, author, date in group:
        p_commit = f"* [{summary}](https://projects.blender.org/blender/blender/commit/{commit_hash}) - {author}"
        notes_content.append(p_commit)

        nr_commits += 1

stats_total = f"{nr_commits} commits in total, of which {nr_commits_fix} are fixes and {nr_commits_cleanup} cleanup.\n"

with open(notes_file, 'w') as f:
    import os 

    f.write(f'\n{stats_total}\n')
    f.write('\n'.join(notes_content))
    notes_file_path = os.path.realpath(f.name)

print(stats_total)
print(f"Logs saved to: {notes_file_path}")