# Parsero
Utility to parse commits and make a nice list, grouped by topic, in a markdown file.

## Requirements
* `git`
* `python`:
    * Git module (`pip install gitpython`)
    * pytz module (`pip install pytz`)
* A copy of [Blender's repository](https://projects.blender.org/blender/blender) or whatever repo you want to check.

## Usage
* Clone this repository
* Run `./parsero.py` with the following arguments:
    1. Path to the repository you want to check for commits.
    2. Date (`%Y-%m-%d` format) of when to start to save the commit logs.
    3. End date of until when to check for commit logs.

For example:
`./parsero.py ~/blender-git 2024-2-12 2024-2-19`, will check for git commits in the folder `blender-git` in home, between the 12th of February 2024, until the 19th.

Note: the time of day is hardcoded to 14hs (2PM). It's an arbitrary number that was convenient for me but you can change it by editing the source.

The logs will go into a folder called `commit-logs` next to `parsero.py`, also editable via the source.
